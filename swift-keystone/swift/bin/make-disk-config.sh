#!/bin/bash

if [ -z "$1" ]; then
    echo -e "SWIFT_DISK_SIZE is a mandatory first argument.\nSee manual for truncate command SIZE variable for more details accepted values"
    exit 1
else
    SWIFT_DISK_SIZE=$1
fi

# Some missed commands on build for OpenStack Swift services
truncate -s SWIFT_DISK_SIZE /srv/swift-disk
mkfs.xfs /srv/swift-disk
# Dcoker doesn't suport mount command so these two following commands will take no effect. I believe this disk should be mounted in the host machine and passed as -v flag on the `docker run` command previously executed
# sudo echo '/srv/swift-disk /mnt/sdb1 xfs loop,noatime,nodiratime,nobarrier,logbufs=8 0 0' >> /etc/fstab
# mount /mnt/sdb1
mkdir -p /swift/nodes/1
ln -s /swift/nodes/1 /srv/1
mkdir -p /srv/1/node/sdb1 /var/run/swift
/usr/bin/sudo /bin/chown -R swift:swift /swift/nodes /etc/swift /var/run/swift /srv/1
/usr/bin/sudo -u swift /swift/bin/remakerings
service swift-proxy restart
service swift-container restart
service swift-account restart
service swift-object restart
